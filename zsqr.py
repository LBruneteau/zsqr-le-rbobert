import os
import json
import dotenv
import asyncio
import datetime
import zipfile
from discord.ext import commands

dotenv.load_dotenv()

TOKEN = os.getenv('TOKEN')
ADMINS = {int(os.getenv('LWI')), int(os.getenv('ECCHOU')), int(os.getenv('MERLIN'))}
CHEMIN_DICO = "dico.json"
OUI = '✅'
NON = '❎'
FEUR = False


with open(CHEMIN_DICO, 'r') as f:
    try:
        dico = json.load(f)
    except json.JSONDecodeError:
        dico = {}

def save_dico():
    with open(CHEMIN_DICO, 'w') as f:
        f.write(json.dumps(dico, indent=4))

save_dico()

bot = commands.Bot(command_prefix='%')

@bot.command(
    name='add',
    help="Permet d'ajouter une définition au dictionnaire, ou à la queue"
)
async def add(ctx, mot: str, *, definition: str = ''):
    try:
        global dico
        mot = mot.lower()
        if not definition:
            if mot in dico:
                await ctx.reply(f'Le mot "{mot}" est déjà dans le dictionnaire')
            else:
                dico[mot] = ''
                save_dico()
                await ctx.reply(f'Le mot "{mot}" est ajouté à la queue')
            return
        if mot not in dico or dico[mot] == '':
            dico[mot] = str(definition)
            save_dico()
            await ctx.reply(f'Le mot "{mot}" est ajouté')
            return
        msg = await ctx.reply(f'Le mot "{mot}" existe déjà. Voulez-vous le remplacer ?')
        await msg.add_reaction(OUI)
        await msg.add_reaction(NON)
        try:
            reac, _ = await bot.wait_for('reaction_add', check=lambda x, y: x.message.id == msg.id and str(x.emoji) in {OUI, NON} and y.id == ctx.author.id, timeout=30)
            if str(reac) == OUI:
                dico[mot] = str(definition)
                save_dico()
                await ctx.send(f'La définition du mot "{mot}" a été mise à jour')
            else:
                await ctx.send('Modification annulée')
        except asyncio.TimeoutError:
            await ctx.send('Modification annulée')
    except Exception as e:
        await ctx.reply(e)

@bot.command(name='def', help='donne la définition d\'un mot')
async def defi(ctx, *, mot: str):
    mot = mot.lower()
    if mot in dico:
        val = dico[mot]
        if not val:
            await ctx.reply(f'Le mot "{mot}" n\'a pas encore de définition')
        else:
            await ctx.reply(f'{mot} : *{val}*')
        return
    msg = await ctx.reply(f'Le mot "{mot}" n\'est pas encore dans la queue. Voulez-vous l\'ajouter à la queue ?')
    await msg.add_reaction(OUI)
    await msg.add_reaction(NON)
    try:
        reac, _ = await bot.wait_for('reaction_add', check=lambda x, y: x.message.id == msg.id and str(x.emoji) in {OUI, NON} and y.id == ctx.author.id, timeout=30)
        if str(reac) == OUI:
            dico[mot] = ""
            save_dico()
            await ctx.send(f'Le mot "{mot}" est ajouté à la queue')
        else:
            await ctx.send('Annulation')
    except asyncio.TimeoutError:
        await ctx.send('Annulation')

@bot.command(name='queue', help='Affiche la liste des mots en attente d\'une définition')
async def queue(ctx):
    q = sorted([mot for mot, val in dico.items() if not val])
    if not q:
        await ctx.reply('La queue est vide')
    else:
        await ctx.reply('\n'.join(q))

@bot.command(name='liste', help='affiche la liste des mots dans le dictionnaire')
async def liste(ctx):
    l = sorted([mot for mot, val in dico.items() if val])
    if not l:
        await ctx.reply('Le dictionnaire est vide')
    else:
        await ctx.reply('\n'.join(l))

@bot.command(name='suppr', help='Supprime un mot du dictionnaire')
async def suppr(ctx, *, mot: str):
    mot = mot.lower()
    if mot not in dico:
        await ctx.reply('Ce mot n\'existe déjà pas')
        return
    msg = await ctx.reply(f'Voulez vous vraiment supprimer le mot "{mot}" ?')
    await msg.add_reaction(OUI)
    await msg.add_reaction(NON)
    try:
        reac, _ = await bot.wait_for('reaction_add', check=lambda x, y: x.message.id == msg.id and str(x.emoji) in {OUI, NON} and y.id == ctx.author.id, timeout=30)
        if str(reac) == OUI:
            del dico[mot]
            save_dico()
            await ctx.send(f'Le mot "{mot}" est supprimé')
        else:
            await ctx.send('Annulation')
    except asyncio.TimeoutError:
        await ctx.send('Annulation')

@bot.command(name='feur', help='active / désactive le feur avec on / off, ou affiche son statut actuel')
async def feur(ctx, mode: str = ''):
    global FEUR
    if not mode:
        await ctx.reply(f'Le feur est {"activé" if FEUR else "désactivé"}')
    elif ctx.author.id not in ADMINS:
        ctx.reply('Permission refusée')
    elif mode == 'on':
        FEUR = True
        await ctx.reply('Activation du feur')
    elif mode == 'off':
        FEUR = False
        await ctx.reply('Désactivation du feur')
    else:
        print('non')

async def mess(message):
    msg = ''.join([i for i in message.content.lower() if i.isalpha()])
    if msg.endswith('quoi') and FEUR and not message.author.bot and message.content[0] != '%':
        await message.reply("feur.", mention_author=False)
bot.add_listener(mess, 'on_message')

@bot.command(name="save", help='Crée une backup du dico')
async def creer_backup(ctx):
    if ctx.author.id not in ADMINS:
        await ctx.reply('Permission refusée')
        return
    date = datetime.datetime.today()
    nom = 'backup/' + date.isoformat(sep=" ", timespec='minutes') + '.zip'
    with zipfile.ZipFile(nom, "w") as archive:
        archive.write(CHEMIN_DICO)
    await ctx.reply('Backup créée')
if __name__ == '__main__':
    bot.run(TOKEN)